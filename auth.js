//This is the secret string which will validate or which will use to check the validity of a passed token. IF a token does not contain this secret string, then that token is invalid or illegitimate.
const secret = "courseBookingAPI";

/*
	JWT is a way to securely pass information from one part of a server to the frontend or other parts of our application. This will allow us to authorize our users to access or disallow access certains of our application.

	JWT is like a gift wrapping service which will encode the user's details and can only be unwrapped by jwt's own methods and if the secret is intact.

	IF the jwt seemed tampered will reject the user's attempt to access a feature in our app.
*/

module.exports.createAccessToken = (userDetails) => {

	//Pick only certain details from our user to be included in the token.
	//Password should not be included.
	//console.log(userDetails);

	const data = {
		id: userDetails.id,
		email: userDetails.email,
		isAdmin: userDetails.isAdmin
	}

	console.log(data);

	//jwt.sign() will create a JWT using our data object, with our secret.
	return jwt.sign(data,secret,{});

}


module.exports.verify = (req,res,next) => {

	//verify() is going to be used as a middleware, wherein it will be added per route to act as a gate to check if the token being passed is valid or not.
	//This will also allow us to check if the user is allowed to access the feature or not.
	//We will also check the validity of the token using its secret.

	//We will pass the token with our request headers as authorization.
	//Requests that need a token must be able to pass the token in the authorization headers.
	let token = req.headers.authorization

	//IF token is undefined, then req.headers.authorization is empty. Which means, the request did not pass a token in the authorization headers.
	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No Token."});
	} else {
		console.log(token);
	}




}

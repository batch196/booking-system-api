/*
	TO be able to create routes from another file to be used in our application, from here, we have to import express as well, however, we will now use another method from express to contain our routes.
	the Router() method,will us to contain our routes
*/

const express = require("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");

//All routes to courses now has an endpoint prefaced with /courses

//endpoint - /courses/
router.get('/',courseControllers.getAllCourses);

//endpoint /courses/
router.post('/',courseControllers.addCourse);


module.exports = router;
